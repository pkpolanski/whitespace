#include <cstdio>
#include <cctype>
#include <stack>
#include <vector>
#include <array>
#include <cstdlib>
#include <map>
#include <string>
#include <getopt.h>
char* program;
int programIndex;
int programLength;
int callCnt = 0;

int isComment(char c) {
  return c != ' ' && c != '\t' && c != '\n' && c != EOF;
}

char getncc(FILE* f) {
  char c = getc(f);
  while (isComment(c)) {
    c = getc(f);
  }
  return c;
}

void loadProgram(FILE* f) {
  char c;
  std::vector<char> programVector;
  c = getncc(f);
  while (c != EOF) {
    programVector.push_back(c);
    c = getncc(f);
  }
  program = new char[programVector.size()];
  programLength = programVector.size();
  for (int i = 0; i < programVector.size(); ++i) {
    program[i] = programVector[i];
  }
  programIndex = 0;
}

char getpc() {
  return program[programIndex++];
}

int IMP () {
  char c = getpc();
  if (c == ' ') {
    return 0;
  } else if (c == '\t') {
    c = getpc();
    if (c == ' ') {
      return 1;
    } else if (c == '\t') {
      return 2;
    } else if (c == '\n') {
      return 4;
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file.\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: unable to determine IMP (character: '%c').\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\n') {
    return 3;
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file.\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: unable to determine IMP (character: '%c').\n", c);
    delete [] program;
    exit(1);
  }
}

int readBinaryNumber() {
  int sign;
  int abs = 0;
  char c = getpc();
  if (c == '\n') {
    return 0;
  } else if (c == ' ') {
    sign = 1;
  } else if (c == '\t') {
    sign = -1;
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (binary number read).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character in binary number read: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
  while (1) {
    c = getpc();
    if (c == '\n') {
      return sign * abs;
    }
    abs *= 2;
    if (c == ' ') {
    } else if (c == '\t') {
      ++abs;
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (binary number read).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in binary number read: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  }
}

void stackManipulation() {
  char c = getpc();
  if (c == ' ') {
    int n = readBinaryNumber();
    printf("  programStack.push(%d);\n", n);
  } else if (c == '\n') {
    c = getpc();
    if (c == ' ') {
      puts("  programStack.push(programStack.top());");
    } else if (c == '\t') {
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  belowIt = programStack.top();");
      puts("  programStack.pop();");
      puts("  programStack.push(top);");
      puts("  programStack.push(belowIt);");
    } else if (c == '\n') {
      puts("  programStack.pop();");
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (stack manipulation).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for stack manipulation: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (stack manipulation).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for stack manipulation: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

void arithmetic() {
  int a, b;
  char c = getpc();
  if (c == ' ') {
    c = getpc();
    if (c == ' ') {
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  belowIt = programStack.top();");
      puts("  programStack.pop();");
      puts("  programStack.push(belowIt + top);");
    } else if (c == '\t') {
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  belowIt = programStack.top();");
      puts("  programStack.pop();");
      puts("  programStack.push(belowIt - top);");
    } else if (c == '\n') {
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  belowIt = programStack.top();");
      puts("  programStack.pop();");
      puts("  programStack.push(belowIt * top);");
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (arithmetic).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for arithmetic operation: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\t') {
    c = getpc();
    if (c == ' ') {
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  belowIt = programStack.top();");
      puts("  programStack.pop();");
      puts("  programStack.push(belowIt / top);");
    } else if (c == '\t') {
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  belowIt = programStack.top();");
      puts("  programStack.pop();");
      puts("  programStack.push(belowIt % top);");
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (arithmetic).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for arithmetic operation: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (arithmetic).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for arithmetic operation: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

void heapAccess() {
  int address, value;
  char c = getpc();
  if (c == ' ') {
    puts("  top = programStack.top();");
    puts("  programStack.pop();");
    puts("  belowIt = programStack.top();");
    puts("  programStack.pop();");
    puts("  heap[belowIt] = top;");
  } else if (c == '\t') {
    puts("  top = programStack.top();");
    puts("  programStack.pop();");
    puts("  programStack.push(heap[top]);");
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (heap access).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for heap access: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

std::string readLabel() {
  std::string val;
  char c;
  while (1) {
    c = getpc();
    if (c == '\n') {
      return val;
    }
    if (c == ' ') {
      val += '0';
    } else if (c == '\t') {
      val += '1';
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (binary number read).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in binary number read: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  }
}

const char* hrf(std::string s) {
  std::string ts = "label_" + s;
  return ts.c_str();
  switch (s.size() % 4) {
    case 1:
      ts = "000" + s;
      break;
    case 2:
      ts = "00" + s;
      break;
    case 3:
      ts = "0" + s;
      break;
    default:
      ts = s;
      break;
  }
  std::string hex;
  for (int i = 0; i < ts.size() / 4; ++i) {
    if (ts[4 * i] == '0') {
      if (ts[4 * i + 1] == '0') {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 0000
            hex += '0';
          } else { // 0001
            hex += '1';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 0010
            hex += '2';
          } else { // 0011
            hex += '3';
          }
        }
      } else {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 0100
            hex += '4';
          } else { // 0101
            hex += '5';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 0110
            hex += '6';
          } else { // 0111
            hex += '7';
          }
        }
      }
    } else {
      if (ts[4 * i + 1] == '0') {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 1000
            hex += '8';
          } else { // 1001
            hex += '9';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 1010
            hex += 'A';
          } else { // 1011
            hex += 'B';
          }
        }
      } else {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 1100
            hex += 'C';
          } else { // 1101
            hex += 'D';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 1110
            hex += 'E';
          } else { // 1111
            hex += 'F';
          }
        }
      }
    }
  }
  if (hex.size() % 2) {
    return hex.c_str();
  }
  std::string ascii;
  for (int i = 0; i < hex.size() / 2; ++i) {
    unsigned char c;
    unsigned char h, o;
    if (isdigit(hex[2 * i])) {
      h = hex[2 * i] - '0';
    } else {
      h = hex[2 * i] - 'A' + 10;
    }
    if (isdigit(hex[2 * i + 1])) {
      o = hex[2 * i + 1] - '0';
    } else {
      o = hex[2 * i + 1] - 'A' + 10;
    }
    c = 16 * h + o;
    if (isprint(c)) {
      ascii += c;
    } else {
      return hex.c_str();
    }
  }
  return ascii.c_str();
}

void flowControl() {
  std::string label;
  char c = getpc();
  if (c == ' ') {
    c = getpc();
    label = readLabel();
    if (c == ' ') {
      printf("  %s:\n", hrf(label));
    } else if (c == '\t') {
      printf("  callStack.push(&&afterCall%d);\n", ++callCnt);
      printf("  goto %s;\n", hrf(label));
      printf("  afterCall%d:\n", callCnt);
    } else if (c == '\n') {
      printf("  goto %s;\n", hrf(label));
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (flow control).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for flow control: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\t') {
    c = getpc();
    int top;
    if (c == ' ') {
      label = readLabel();
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  if (!top) {");
      printf("    goto %s;\n", hrf(label));
      puts("  }");
    } else if (c == '\t') {
      label = readLabel();
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  if (top < 0) {");
      printf("    goto %s;\n", hrf(label));
      puts("  }");
    } else if (c == '\n') {
      puts("  ptr = callStack.top();");
      puts("  callStack.pop();");
      puts("  goto *ptr;");
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (flow control).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for flow control: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\n') {
    c = getpc();
    if (c == '\n') {
      puts("  return 0;");
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (flow control).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for flow control: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (flow control).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for flow control: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

void io() {
  int n;
  int address;
  char c = getpc();
  if (c == ' ') {
    c = getpc();
    if (c == ' ') {
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  printf(\"%c\", top);");
    } else if (c == '\t') {
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  printf(\"%d\", top);");
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (I/O).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in I/O: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\t') {
    c = getpc();
    if (c == ' ') {
      puts("  c = getc(stdin);");
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  heap[top] = c;");
    } else if (c == '\t') {
      puts("  scanf(\"%d\", &n);");
      puts("  top = programStack.top();");
      puts("  programStack.pop();");
      puts("  heap[top] = n;");
      puts("  getc(stdin);");
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (I/O).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in I/O: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (I/O).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character in I/O: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

int main(int argc, char** argv) {
  puts("#include <cstdio>");
  puts("#include <cctype>");
  puts("#include <stack>");
  puts("#include <vector>");
  puts("#include <cstdlib>");
  puts("#include <map>");
  puts("#include <string>");
  puts("int main() {");
  puts("  std::stack<int> programStack;");
  puts("  std::stack<void*> callStack;");
  puts("  std::map<int, int> heap;");
  puts("  int top;");
  puts("  int belowIt;");
  puts("  void *ptr;");
  puts("  char c;");
  puts("  int n;");
  if (argc < 2) {
    printf("No filename given.\n");
    exit(1);
  }
  FILE* f = fopen(argv[optind], "r");
  loadProgram(f);
  fclose(f);
  int imp;
  while (1) {
    if (programIndex >= programLength) {
      break;
    }
    imp = IMP();
    if (imp == 0) {
      stackManipulation();
    } else if (imp == 1) {
      arithmetic();
    } else if (imp == 2) {
      heapAccess();
    } else if (imp == 3) {
      flowControl();
    } else if (imp == 4) {
      io();
    }
  }
  puts("}");
}

#include <cstdio>
#include <cctype>
#include <stack>
#include <vector>
#include <cstdlib>
#include <map>
#include <string>
#include <getopt.h>
std::stack<int> programStack;
std::stack<int> callStack;
char* program;
std::map<int, int> heap;
std::map<std::string, int> labels;
std::string wsv = "0.2";
int programIndex;
int programLength;
int ready;

int isComment(char c) {
  return c != ' ' && c != '\t' && c != '\n' && c != EOF;
}

char getncc(FILE* f) {
  char c = getc(f);
  while (isComment(c)) {
    c = getc(f);
  }
  return c;
}

void loadProgram(FILE* f) {
  char c;
  std::vector<char> programVector;
  c = getncc(f);
  while (c != EOF) {
    programVector.push_back(c);
    c = getncc(f);
  }
  program = new char[programVector.size()];
  programLength = programVector.size();
  for (int i = 0; i < programVector.size(); ++i) {
    program[i] = programVector[i];
  }
  programIndex = 0;
}

char getpc() {
  return program[programIndex++];
}

int IMP () {
  char c = getpc();
  if (c == ' ') {
    return 0;
  } else if (c == '\t') {
    c = getpc();
    if (c == ' ') {
      return 1;
    } else if (c == '\t') {
      return 2;
    } else if (c == '\n') {
      return 4;
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file.\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: unable to determine IMP (character: '%c').\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\n') {
    return 3;
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file.\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: unable to determine IMP (character: '%c').\n", c);
    delete [] program;
    exit(1);
  }
}

int readBinaryNumber() {
  int sign;
  int abs = 0;
  char c = getpc();
  if (c == '\n') {
    return 0;
  } else if (c == ' ') {
    sign = 1;
  } else if (c == '\t') {
    sign = -1;
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (binary number read).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character in binary number read: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
  while (1) {
    c = getpc();
    if (c == '\n') {
      return sign * abs;
    }
    abs *= 2;
    if (c == ' ') {
    } else if (c == '\t') {
      ++abs;
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (binary number read).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in binary number read: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  }
}

void stackManipulation(int prepare = 0) {
  char c = getpc();
  if (c == ' ') {
    int n = readBinaryNumber();
    if (!prepare) {
      programStack.push(n);
    }
  } else if (c == '\t' && wsv == "0.3") {
    c = getpc();
    if (c == ' ') {
      // Copy the nth item on the stack (given by the argument) onto the top of the stack
      // zakładam, że 0 oznacza wierzchni element
      int n = readBinaryNumber();
      if (!prepare) {
        if (programStack.size() < n + 1) {
          printf("Compile error: stack too small to copy %dth element on top.\n", n);
          delete [] program;
          exit(1);
        }
        int toCopy;
        std::stack<int> auxStack;
        if (0 == n) {
          programStack.push(programStack.top());
        } else {
          for (int i = 0; i < n; ++i) {
            auxStack.push(programStack.top());
            programStack.pop();
          }
          toCopy = programStack.top();
          for (int i = 0; i < n; ++i) {
            programStack.push(auxStack.top());
            auxStack.pop();
          }
        }
      }
    } else if (c == '\n') {
      // Slide n items off the stack, keeping the top item
      int n = readBinaryNumber();
      if (!prepare) {
        if (n && programStack.size() < n + 1) {
          printf("Compile error: stack too small to slide %d elements.\n", n);
          delete [] program;
          exit(1);
        }
        int toKeep = programStack.top();
        for (int i = 0; i <= n; ++i) {
          programStack.pop();
        }
        programStack.push(toKeep);
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (stack manipulation).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for stack manipulation: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\n') {
    c = getpc();
    if (c == ' ') {
      if (!prepare) {
        if (programStack.empty()) {
          printf("Compile error: stack is empty, duplicate requested.\n");
          delete [] program;
          exit(1);
        }
        programStack.push(programStack.top());
      }
    } else if (c == '\t') {
      if (!prepare) {
        if (programStack.size() < 2) {
          printf("Compile error: stack too small to swap two top elements.\n");
          delete [] program;
          exit(1);
        } else {
          int top = programStack.top();
          programStack.pop();
          int belowIt = programStack.top();
          programStack.pop();
          programStack.push(top);
          programStack.push(belowIt);
        }
      }
    } else if (c == '\n') {
      if (!prepare) {
        if (programStack.empty()) {
          printf("Compile error: stack is empty, pop requested.\n");
          delete [] program;
          exit(1);
        } else {
          programStack.pop();
        }
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (stack manipulation).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for stack manipulation: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (stack manipulation).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for stack manipulation: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

void arithmetic(int prepare = 0) {
  int a, b;
  if (!prepare) {
    if (programStack.size() < 2) { 
      printf("Compile error: stack too small for arithmetic operation.\n");
      delete [] program;
      exit(1);
    }
    b = programStack.top();
    programStack.pop();
    a = programStack.top();
    programStack.pop();
  }
  char c = getpc();
  if (c == ' ') {
    c = getpc();
    if (c == ' ') {
      if (!prepare) {
        programStack.push(a + b);
      }
    } else if (c == '\t') {
      if (!prepare) {
        programStack.push(a - b);
      }
    } else if (c == '\n') {
      if (!prepare) {
        programStack.push(a * b);
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (arithmetic).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for arithmetic operation: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\t') {
    c = getpc();
    if (c == ' ') {
      if (!prepare) {
        programStack.push(a / b);
      }
    } else if (c == '\t') {
      if (!prepare) {
        programStack.push(a % b);
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (arithmetic).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for arithmetic operation: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (arithmetic).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for arithmetic operation: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

void heapAccess(int prepare = 0) {
  int address, value;
  char c = getpc();
  if (c == ' ') {
    if (!prepare) {
      if (programStack.size() < 2) { 
        printf("Compile error: stack too small for store operation: %d.\n", programIndex);
        delete [] program;
        exit(1);
      }
      value = programStack.top();
      programStack.pop();
      address = programStack.top();
      programStack.pop();
      heap[address] = value;
    }
  } else if (c == '\t') {
    if (!prepare) {
        if (programStack.size() < 1) { 
          printf("Compile error: stack too small for retrieve operation: %d.\n", programIndex);
          delete [] program;
          exit(1);
        }
      address = programStack.top();
      programStack.pop();
      programStack.push(heap[address]);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (heap access).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for heap access: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

void mark(std::string label) {
  if (labels.count(label)) {
    printf("Compile error: duplicate label given.\n");
    printf("Program index: %d, label: %s.\n", programIndex, label.c_str());
    delete [] program;
    exit(1);
  }
  labels[label] = programIndex;
}

void call(std::string label) {
  callStack.push(programIndex);
  if (labels.count(label)) {
    programIndex = labels[label];
  } else {
    printf("Compile error: no such label (%s) during call.\n", label.c_str());
    delete [] program;
    exit(1);
  }
}

void jumpTo(std::string label) {
  if (labels.count(label)) {
    programIndex = labels[label];
  } else {
    printf("Compile error: no such label (%s) during jump.\n", label.c_str());
    delete [] program;
    exit(1);
  }
}

std::string readLabel() {
  std::string val;
  char c;
  while (1) {
    c = getpc();
    if (c == '\n') {
      return val;
    }
    if (c == ' ') {
      val += '0';
    } else if (c == '\t') {
      val += '1';
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (binary number read).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in binary number read: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  }
}

const char* hrf(std::string s) {
  std::string ts;
  switch (s.size() % 4) {
    case 1:
      ts = "000" + s;
      break;
    case 2:
      ts = "00" + s;
      break;
    case 3:
      ts = "0" + s;
      break;
    default:
      ts = s;
      break;
  }
  std::string hex;
  for (int i = 0; i < ts.size() / 4; ++i) {
    if (ts[4 * i] == '0') {
      if (ts[4 * i + 1] == '0') {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 0000
            hex += '0';
          } else { // 0001
            hex += '1';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 0010
            hex += '2';
          } else { // 0011
            hex += '3';
          }
        }
      } else {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 0100
            hex += '4';
          } else { // 0101
            hex += '5';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 0110
            hex += '6';
          } else { // 0111
            hex += '7';
          }
        }
      }
    } else {
      if (ts[4 * i + 1] == '0') {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 1000
            hex += '8';
          } else { // 1001
            hex += '9';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 1010
            hex += 'A';
          } else { // 1011
            hex += 'B';
          }
        }
      } else {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 1100
            hex += 'C';
          } else { // 1101
            hex += 'D';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 1110
            hex += 'E';
          } else { // 1111
            hex += 'F';
          }
        }
      }
    }
  }
  if (hex.size() % 2) {
    return hex.c_str();
  }
  std::string ascii;
  for (int i = 0; i < hex.size() / 2; ++i) {
    unsigned char c;
    unsigned char h, o;
    if (isdigit(hex[2 * i])) {
      h = hex[2 * i] - '0';
    } else {
      h = hex[2 * i] - 'A' + 10;
    }
    if (isdigit(hex[2 * i + 1])) {
      o = hex[2 * i + 1] - '0';
    } else {
      o = hex[2 * i + 1] - 'A' + 10;
    }
    c = 16 * h + o;
    if (isprint(c)) {
      ascii += c;
    } else {
      return hex.c_str();
    }
  }
  return ascii.c_str();
}

void flowControl(int prepare = 0) {
  std::string label;
  char c = getpc();
  if (c == ' ') {
    c = getpc();
    label = readLabel();
    if (c == ' ') {
      if (prepare) {
        mark(label);
      }
    } else if (c == '\t') {
      if (!prepare) {
        call(label);
      }
    } else if (c == '\n') {
      if (!prepare) {
        jumpTo(label);
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (flow control).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for flow control: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\t') {
    c = getpc();
    int top;
    if (c == ' ') {
      label = readLabel();
      if (!prepare) {
        if (programStack.empty()) {
          printf("Compile error: stack is empty, jumpz requested.\n");
          delete [] program;
          exit(1);
        }
        top = programStack.top();
        programStack.pop();
        if (!top) {
          jumpTo(label);
        }
      }
    } else if (c == '\t') {
      label = readLabel();
      if (!prepare) {
        if (programStack.empty()) {
          printf("Compile error: stack is empty, jumpz requested.\n");
          delete [] program;
          exit(1);
        }
        top = programStack.top();
        programStack.pop();
        if (top < 0) {
          jumpTo(label);
        }
      }
    } else if (c == '\n') {
      if (!prepare) {
        if (callStack.empty()) {
          printf("Compile error: request to end a subroutine while no subroutine is running.\n");
          delete [] program;
          exit(1);
        }
        programIndex = callStack.top();
        callStack.pop();
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (flow control).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for flow control: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\n') {
    c = getpc();
    if (c == '\n') {
      if (!prepare) {
        delete [] program;
        exit(0);
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (flow control).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for flow control: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (flow control).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for flow control: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

void io(int prepare = 0) {
  int n;
  int address;
  char c = getpc();
  if (c == ' ') {
    c = getpc();
    if (c == ' ') {
      if (!prepare) {
        if (programStack.empty()) {
          printf("Compile error: stack is empty, outc requested.\n");
          delete [] program;
          exit(1);
        }
        printf("%c", programStack.top());
        programStack.pop();
      }
    } else if (c == '\t') {
      if (!prepare) {
        if (programStack.empty()) {
          printf("Compile error: stack is empty, outn requested.\n");
          delete [] program;
          exit(1);
        }
        printf("%d", programStack.top());
        programStack.pop();
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (I/O).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in I/O: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\t') {
    c = getpc();
    if (c == ' ') {
      if (!prepare) {
        if (programStack.empty()) {
          printf("Compile error: stack is empty, inc requested.\n");
          delete [] program;
          exit(1);
        }
        c = getc(stdin);
        address = programStack.top();
        programStack.pop();
        heap[address] = c;
      }
    } else if (c == '\t') {
      if (!prepare) {
        if (programStack.empty()) {
          printf("Compile error: stack is empty, inn requested.\n");
          delete [] program;
          exit(1);
        }
        scanf("%d", &n);
        address = programStack.top();
        programStack.pop();
        heap[address] = n;
        getc(stdin);
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (I/O).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in I/O: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (I/O).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character in I/O: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

int main(int argc, char** argv) {
  int c;
  while ((c = getopt(argc, argv, "w:") != -1)) {
    switch (c) {
      case 'w':
        if (std::string("0.3") == optarg) {
          wsv = optarg;
        }
        break;
      default:
        break;
    }
  }

  if (argc < 2) {
    printf("No filename given.\n");
    exit(1);
  }
  FILE* f = fopen(argv[1], "r");
  loadProgram(f);
  fclose(f);
  int imp;
  ready = 0;
  while (1) {
    if (programIndex >= programLength) {
      break;
    }
    imp = IMP();
    if (imp == 0) {
      stackManipulation(1);
    } else if (imp == 1) {
      arithmetic(1);
    } else if (imp == 2) {
      heapAccess(1);
    } else if (imp == 3) {
      flowControl(1);
    } else if (imp == 4) {
      io(1);
    }
  }
  programIndex = 0;
  imp = IMP();
  while (1) {
    if (imp == 0) {
      stackManipulation();
    } else if (imp == 1) {
      arithmetic();
    } else if (imp == 2) {
      heapAccess();
    } else if (imp == 3) {
      flowControl();
    } else if (imp == 4) {
      io();
    }
    imp = IMP();
  }
  return 0;
}

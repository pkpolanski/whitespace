#include <cstdio>
#include <cctype>
#include <stack>
#include <vector>
#include <array>
#include <cstdlib>
#include <map>
#include <string>
std::stack<int> programStack;
std::stack<int> callStack;
char* program;
std::map<int, int> heap;
std::map<std::string, int> labels;
int programIndex;
int programLength;
int ready;

int isComment(char c) {
  return c != ' ' && c != '\t' && c != '\n' && c != EOF;
}

char getncc(FILE* f) {
  char c = getc(f);
  while (isComment(c)) {
    c = getc(f);
  }
  return c;
}

void loadProgram(FILE* f) {
  char c;
  std::vector<char> programVector;
  c = getncc(f);
  while (c != EOF) {
    programVector.push_back(c);
    c = getncc(f);
  }
  program = new char[programVector.size()];
  programLength = programVector.size();
  for (int i = 0; i < programVector.size(); ++i) {
    program[i] = programVector[i];
  }
  programIndex = 0;
}

char getpc() {
  return program[programIndex++];
}

int IMP () {
  char c = getpc();
  if (c == ' ') {
    return 0;
  } else if (c == '\t') {
    c = getpc();
    if (c == ' ') {
      return 1;
    } else if (c == '\t') {
      return 2;
    } else if (c == '\n') {
      return 4;
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file.\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: unable to determine IMP (character: '%c').\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\n') {
    return 3;
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file.\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: unable to determine IMP (character: '%c').\n", c);
    delete [] program;
    exit(1);
  }
}

int readBinaryNumber() {
  int sign;
  int abs = 0;
  char c = getpc();
  if (c == '\n') {
    return 0;
  } else if (c == ' ') {
    sign = 1;
  } else if (c == '\t') {
    sign = -1;
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (binary number read).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character in binary number read: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
  while (1) {
    c = getpc();
    if (c == '\n') {
      return sign * abs;
    }
    abs *= 2;
    if (c == ' ') {
    } else if (c == '\t') {
      ++abs;
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (binary number read).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in binary number read: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  }
}

void stackManipulation(int skip = 0) {
  char c = getpc();
  if (c == ' ') {
    int n = readBinaryNumber();
    if (skip) {
      printf("PUSH %d\n", n);
    } else {
      programStack.push(n);
    }
  } else if (c == '\n') {
    c = getpc();
    if (c == ' ') {
      if (skip) {
        printf("DUPLICATE\n");
      } else {
        programStack.push(programStack.top());
      }
    } else if (c == '\t') {
      if (skip) {
        printf("SWAP\n");
      } else{
        if (programStack.size() > 1) {
          int top = programStack.top();
          programStack.pop();
          int belowIt = programStack.top();
          programStack.pop();
          programStack.push(top);
          programStack.push(belowIt);
        } else {
          printf("Compile error: stack too small to swap two top elements.\n");
          delete [] program;
          exit(1);
        }
      }
    } else if (c == '\n') {
      if (skip) {
        printf("POP\n");
      } else {
        if (programStack.empty()) {
          printf("Compile error: stack is empty, pop requested.\n");
          delete [] program;
          exit(1);
        } else {
          programStack.pop();
        }
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (stack manipulation).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for stack manipulation: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (stack manipulation).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for stack manipulation: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

void arithmetic(int skip = 0) {
  int a, b;
  if (!skip) {
    if (programStack.size() < 2) { 
      printf("Compile error: stack too small for arithmetic operation.\n");
      delete [] program;
      exit(1);
    }
    b = programStack.top();
    programStack.pop();
    a = programStack.top();
    programStack.pop();
  }
  char c = getpc();
  if (c == ' ') {
    c = getpc();
    if (c == ' ') {
      if (skip) {
        printf("ADD\n");
      } else {
        programStack.push(a + b);
      }
    } else if (c == '\t') {
      if (skip) {
        printf("SUB\n");
      } else {
        programStack.push(a - b);
      }
    } else if (c == '\n') {
      if (skip) {
        printf("MUL\n");
      } else {
        programStack.push(a * b);
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (arithmetic).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for arithmetic operation: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\t') {
    c = getpc();
    if (c == ' ') {
      if (skip) {
        printf("DIV\n");
      } else {
        programStack.push(a / b);
      }
    } else if (c == '\t') {
      if (skip) {
        printf("MOD\n");
      } else {
        programStack.push(a % b);
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (arithmetic).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for arithmetic operation: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (arithmetic).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for arithmetic operation: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

void heapAccess(int skip = 0) {
  int address, value;
  char c = getpc();
  if (c == ' ') {
    if (skip) {
      printf("STORE\n");
    } else {
      if (!skip) {
        if (programStack.size() < 2) { 
          printf("Compile error: stack too small for store operation: %d.\n", programIndex);
          delete [] program;
          exit(1);
        }
      }
      value = programStack.top();
      programStack.pop();
      address = programStack.top();
      programStack.pop();
      heap[address] = value;
    }
  } else if (c == '\t') {
    if (skip) {
      printf("%d: RETRIEVE\n", programIndex - 3);
    } else {
      if (!skip) {
        if (programStack.size() < 1) { 
          printf("Compile error: stack too small for retrieve operation: %d.\n", programIndex);
          delete [] program;
          exit(1);
        }
      }
      address = programStack.top();
      programStack.pop();
      programStack.push(heap[address]);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (heap access).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for heap access: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

void mark(std::string label) {
  if (labels.count(label)) {
    printf("Compile error: duplicate label given.\n");
    printf("Program index: %d, label: %s.\n", programIndex, label.c_str());
    delete [] program;
    exit(1);
  }
  labels[label] = programIndex;
}

void call(std::string label) {
  callStack.push(programIndex);
  if (labels.count(label)) {
    programIndex = labels[label];
  } else {
    printf("Compile error: no such label (%s) during call.\n", label.c_str());
    delete [] program;
    exit(1);
  }
}

void jumpTo(std::string label) {
  if (labels.count(label)) {
    programIndex = labels[label];
  } else {
    printf("Compile error: no such label (%s) during jump.\n", label.c_str());
    delete [] program;
    exit(1);
  }
}

std::string readLabel() {
  std::string val;
  char c;
  while (1) {
    c = getpc();
    if (c == '\n') {
      return val;
    }
    if (c == ' ') {
      val += '0';
    } else if (c == '\t') {
      val += '1';
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (binary number read).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in binary number read: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  }
}

const char* hrf(std::string s) {
  std::string ts;
  switch (s.size() % 4) {
    case 1:
      ts = "000" + s;
      break;
    case 2:
      ts = "00" + s;
      break;
    case 3:
      ts = "0" + s;
      break;
    default:
      ts = s;
      break;
  }
  std::string hex;
  for (int i = 0; i < ts.size() / 4; ++i) {
    if (ts[4 * i] == '0') {
      if (ts[4 * i + 1] == '0') {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 0000
            hex += '0';
          } else { // 0001
            hex += '1';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 0010
            hex += '2';
          } else { // 0011
            hex += '3';
          }
        }
      } else {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 0100
            hex += '4';
          } else { // 0101
            hex += '5';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 0110
            hex += '6';
          } else { // 0111
            hex += '7';
          }
        }
      }
    } else {
      if (ts[4 * i + 1] == '0') {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 1000
            hex += '8';
          } else { // 1001
            hex += '9';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 1010
            hex += 'A';
          } else { // 1011
            hex += 'B';
          }
        }
      } else {
        if (ts[4 * i + 2] == '0') {
          if (ts[4 * i + 3] == '0') { // 1100
            hex += 'C';
          } else { // 1101
            hex += 'D';
          }
        } else {
          if (ts[4 * i + 3] == '0') { // 1110
            hex += 'E';
          } else { // 1111
            hex += 'F';
          }
        }
      }
    }
  }
  if (hex.size() % 2) {
    return hex.c_str();
  }
  std::string ascii;
  for (int i = 0; i < hex.size() / 2; ++i) {
    unsigned char c;
    unsigned char h, o;
    if (isdigit(hex[2 * i])) {
      h = hex[2 * i] - '0';
    } else {
      h = hex[2 * i] - 'A' + 10;
    }
    if (isdigit(hex[2 * i + 1])) {
      o = hex[2 * i + 1] - '0';
    } else {
      o = hex[2 * i + 1] - 'A' + 10;
    }
    c = 16 * h + o;
    if (isprint(c)) {
      ascii += c;
    } else {
      return hex.c_str();
    }
  }
  return ascii.c_str();
}

void flowControl(int skip = 0) {
  std::string label;
  char c = getpc();
  if (c == ' ') {
    c = getpc();
    label = readLabel();
    if (c == ' ') {
      if (skip) {
        printf("MARK %s\n", hrf(label));
        mark(label);
      }
    } else if (c == '\t') {
      if (skip) {
        printf("CALL %s\n", hrf(label));
      } else {
        call(label);
      }
    } else if (c == '\n') {
      if (skip) {
        printf("GOTO %s\n", hrf(label));
      } else {
        jumpTo(label);
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (flow control).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for flow control: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\t') {
    c = getpc();
    int top;
    if (c == ' ') {
      label = readLabel();
      if (skip) {
        printf ("JZ %s\n", hrf(label));
      } else {
        top = programStack.top();
        programStack.pop();
        if (!top) {
          jumpTo(label);
        }
      }
    } else if (c == '\t') {
      label = readLabel();
      if (skip) {
        printf("JN %s\n", hrf(label));
      } else {
        top = programStack.top();
        programStack.pop();
        if (top < 0) {
          jumpTo(label);
        }
      }
    } else if (c == '\n') {
      if (skip) {
        printf("RETI\n");
      } else {
        if (callStack.empty()) {
          printf("Compile error: request to end a subroutine while no subroutine is running.\n");
          delete [] program;
          exit(1);
        }
        programIndex = callStack.top();
        callStack.pop();
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (flow control).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for flow control: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\n') {
    c = getpc();
    if (c == '\n') {
      if (skip) {
        printf("RET\n");
      } else {
        delete [] program;
        exit(0);
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (flow control).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character for flow control: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (flow control).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character for flow control: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

void io(int skip = 0) {
  int n;
  int address;
  char c = getpc();
  if (c == ' ') {
    c = getpc();
    if (c == ' ') {
      if (skip) {
        printf("PUTC\n");
      } else {
        printf("%c", programStack.top());
        programStack.pop();
      }
    } else if (c == '\t') {
      if (skip) {
        printf ("PUTN\n");
      } else {
        printf("%d", programStack.top());
        programStack.pop();
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (I/O).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in I/O: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == '\t') {
    c = getpc();
    if (c == ' ') {
      if (skip) {
        printf("%d: GETC\n", programIndex - 4);
      } else {
        c = getc(stdin);
        address = programStack.top();
        programStack.pop();
        heap[address] = c;
      }
    } else if (c == '\t') {
      if (skip) {
        printf("GETN\n");
      } else {
        scanf("%d", &n);
        address = programStack.top();
        programStack.pop();
        heap[address] = n;
        getc(stdin);
        printf("%d\n", n);
      }
    } else if (c == EOF) {
      printf("Compile error: unexpected end of file (I/O).\n");
      delete [] program;
      exit(1);
    } else {
      printf("Compile error: wrong character in I/O: '%c'.\n", c);
      delete [] program;
      exit(1);
    }
  } else if (c == EOF) {
    printf("Compile error: unexpected end of file (I/O).\n");
    delete [] program;
    exit(1);
  } else {
    printf("Compile error: wrong character in I/O: '%c'.\n", c);
    delete [] program;
    exit(1);
  }
}

int main(int argc, char** argv) {
  if (argc < 2) {
    printf("No filename given.\n");
    exit(1);
  }
  FILE* f = fopen(argv[1], "r");
  loadProgram(f);
  //printf("Program loaded (%d non-comment characters).\n", programLength);
  fclose(f);
  int imp;
  ready = 0;
  while (1) {
    if (programIndex >= programLength) {
      break;
    }
    imp = IMP();
    if (imp == 0) {
      stackManipulation(1);
    } else if (imp == 1) {
      arithmetic(1);
    } else if (imp == 2) {
      heapAccess(1);
    } else if (imp == 3) {
      flowControl(1);
    } else if (imp == 4) {
      io(1);
    }
  }
  puts("Program loaded.");
  programIndex = 0;
  imp = IMP();
  while (1) {
    if (imp == 0) {
      stackManipulation();
    } else if (imp == 1) {
      arithmetic();
    } else if (imp == 2) {
      heapAccess();
    } else if (imp == 3) {
      flowControl();
    } else if (imp == 4) {
      io();
    }
    imp = IMP();
  }
  return 0;
}
